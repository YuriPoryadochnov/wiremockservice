package com.epam.wiremockservice.controllers;

import com.epam.wiremockservice.models.Product;
import com.epam.wiremockservice.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/wiremock/product")
public class ProductController {
    @Qualifier("productRepository")
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    Validator productValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(productValidator);
    }

    @GetMapping
    public List<Product> index() {
        return productRepository.findAll();
    }

    @PostMapping
    public Product create(@RequestBody @Valid Product product) {
        return productRepository.save(product);
    }

    @GetMapping("/{id}")
    public Product view(@PathVariable("id") long id) {
        return productRepository.findOne(id);
    }

    @PostMapping(value = "/{id}")
    public Product edit(@PathVariable("id") long id, @RequestBody @Valid Product product) {

        Product updatedProduct = view(id);

        if (updatedProduct == null) {
            return null;
        }

        updatedProduct.setName(product.getName());
        updatedProduct.setPrice(product.getPrice());
        updatedProduct.setDescription(product.getDescription());

        return create(updatedProduct);
    }
}

