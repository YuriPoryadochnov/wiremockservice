package com.epam.wiremockservice;

import com.epam.wiremockservice.validators.ProductValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.Validator;

@EnableEurekaClient
@SpringBootApplication
public class WiremockServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WiremockServiceApplication.class, args);
    }

    @Bean
    public Validator productValidator() {
        return new ProductValidator();
    }

    @Bean
    public Logger getLogger() {
        return LoggerFactory.getLogger("console");
    }
}
